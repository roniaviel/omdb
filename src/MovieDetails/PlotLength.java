package MovieDetails;

public class PlotLength {

	public enum PlotKind{
		Short,
		Full
	}
	
	private PlotKind plotKind;
	
	PlotLength(PlotKind plotKind)
	{
		this.plotKind = plotKind;
	}

	public PlotKind getPlotKind() {
		return plotKind;
	}

	public void setPlotKind(PlotKind plotKind) {
		this.plotKind = plotKind;
	}
	
}
