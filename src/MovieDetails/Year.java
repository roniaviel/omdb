package MovieDetails;

import il.ac.hit.MovieDataServiceException;

public class Year {

	private String year;

	public Year(String year) throws MovieDataServiceException
	{
		setYear(year);
	}

	private void setYear(String year) throws MovieDataServiceException {
		if(year.isEmpty()) 
		{
			return; // The year text box can be empty.
		}
		else if(!isNumeric(year))
		{
			throw new MovieDataServiceException("You must Enter a valid year");
		}
		else if (year.length() != 4 )
		{
			throw new MovieDataServiceException("Oh.. we still live in 4 digits age... Try again");
		}

		int numericYear = Integer.parseInt(year);
		if (!(numericYear >= 1920 &&
				numericYear <=2015))
		{
			throw new MovieDataServiceException("Please Enter a movie between 1920 - 2015");
		}		

	}

	private static boolean isNumeric(String txtBoxMsg) throws MovieDataServiceException
	{
		try
		{
			@SuppressWarnings("unused")
			int numericNumber = Integer.parseInt(txtBoxMsg); 
		}
		catch (NumberFormatException nfe)
		{
			throw new MovieDataServiceException("Please Enter a valid year");
		}
		return true;
	}


	@Override
	public String toString()
	{
		String year = "t="+this.year; 
		return year;
	}

}

