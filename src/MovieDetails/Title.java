package MovieDetails;

import il.ac.hit.MovieDataServiceException;

public class Title {

	private String title;
	
	public Title( String title ) throws MovieDataServiceException
	{
		setTitle(title);
	}

	private void setTitle(String title) throws MovieDataServiceException {
		if(title.equalsIgnoreCase(""))
		{
			throw new MovieDataServiceException("You must Enter a MOVIE title!");
		}
		else if (title.length() == 1 )
		{
			throw new MovieDataServiceException("The movie Title must be more than one letter!");
		}
		
		this.title = title;
		
	}

	@Override
	public String toString()
	{
		String title = "t="+this.title; 
		return title;
	}
	

}
